
PROJNAME := tomasz

all:
	echo "try 'make upload'"

upload:
	rsync -r \
		--exclude .git \
		--exclude Makefile \
		./ \
		dreamhost:artificialworlds.net/presentations/${PROJNAME}/
